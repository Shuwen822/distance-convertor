﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
// Author: Group 4 -- Shuwen, Stephen, Manpreet, Drew
namespace Distance_Converter
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        int sourceSelect;

        int targetSelect;

        string inputString;

        double inputNumber;


        public MainWindow()
        {
            InitializeComponent();
            sourceNumber.Text = "";
            //CheckSourceNumber();
        }

        private void CheckSourceNumber()
        {
            inputString = sourceNumber.Text;
            if (string.IsNullOrEmpty(inputString)){
                message.Content = "Please enter a number";
            } 
            else if(!double.TryParse(inputString, out inputNumber)){
                //Console.WriteLine(inputString);
                message.Content = "Error! Please enter a number";
            }
            else
            {
                message.Content = "";
                //Console.WriteLine(inputNumber);
            }
        }

        private (double, string) stateCheck(int source, int target)
        {
           Dictionary<(int, int), (double,string)> convertState = new Dictionary<(int, int),(double, string)>();
            convertState.Add((0, 0), (1.0, "1 Yard = 1 Yard"));
            convertState.Add((0, 1), (3.0, "1 Yard = 3 Feet"));
            convertState.Add((0, 2), (36.0, "1 Yard = 36 Inches"));
            convertState.Add((0, 3), (0.914, "1 Yard = 0.914 Meter"));
            convertState.Add((1, 0), (0.333, "1 Foot = 0.333 Yard"));
            convertState.Add((1, 1), (1.0, "1 Foot = 1 Foot"));
            convertState.Add((1, 2), (12.0, "1 Foot = 12 Inches"));
            convertState.Add((1, 3), (0.305, "1 Foot = 0.305 Meter"));
            convertState.Add((2, 0), (0.028, "1 Inch = 0.028 Yard"));
            convertState.Add((2, 1), (0.083, "1 Inch = 0.083 Foot"));
            convertState.Add((2, 2), (1.0, "1 Inch = 1 Inch"));
            convertState.Add((2, 3), (0.025, "1 Inch = 0.025 Meter"));
            convertState.Add((3, 0), (1.094, "1 Meter = 1.094 Yard"));
            convertState.Add((3, 1), (3.281, "1 Meter = 3.281 Feet"));
            convertState.Add((3, 2), (39.370, "1 Meter = 39.370 Inches"));
            convertState.Add((3, 3), (1.0, "1 Meter = 1 Meter"));


            sourceSelect = sourceUnitList.SelectedIndex;
            targetSelect = targetUnitList.SelectedIndex;

            if (sourceSelect==-1 || targetSelect == -1) 
            {
                return (-1, "Please select unit");
            }    

            return convertState[(sourceSelect,targetSelect)]; 
        }

        private void Calc_Click(object sender, RoutedEventArgs e)
        {
            CheckSourceNumber();

            sourceSelect = sourceUnitList.SelectedIndex;
            targetSelect = targetUnitList.SelectedIndex;

            var convertIndex = stateCheck(sourceSelect, targetSelect).Item1;
            var convertString = stateCheck(sourceSelect, targetSelect).Item2;

            if (convertIndex == -1)
            {
                resultNumber.Content = "";
                message.Content = convertString;
            }
            //Console.WriteLine(inputNumber);

            else
            {
                decimal resultDisplay = Math.Round(Convert.ToDecimal(inputNumber * convertIndex), 4);
                resultNumber.Content = resultDisplay;
                formula.Content = convertString;
                inputNumber = 0;
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            resultNumber.Content = "";
            message.Content = "";
            sourceNumber.Text = "";
            sourceUnitList.SelectedIndex=-1;
            targetUnitList.SelectedIndex=-1;
        }

        private void sourceNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckSourceNumber();
        }
    }
}
